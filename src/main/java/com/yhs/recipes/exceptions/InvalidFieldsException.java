package com.yhs.recipes.exceptions;

public class InvalidFieldsException extends RuntimeException{
    public InvalidFieldsException(String message) {
        super(message);
    }
}
