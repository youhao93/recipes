package com.yhs.recipes.models;


import javax.persistence.*;
import java.util.List;

@Entity
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    String title;
    boolean vegetarian;
    int servings;

    @ElementCollection
    List<String> ingredients;
    @ElementCollection
    List<String> instructions;


    public Recipe(Long id,
                  String title,
                  boolean vegetarian,
                  int servings,
                  List<String> ingredients,
                  List<String> instructions) {
        this.id = id;
        this.title = title;
        this.vegetarian = vegetarian;
        this.servings = servings;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public Recipe(String title,
                  boolean vegetarian,
                  int servings,
                  List<String> ingredients,
                  List<String> instructions) {
        this.title = title;
        this.vegetarian = vegetarian;
        this.servings = servings;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public Recipe() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public int getServings() {
        return servings;
    }

    public void setServings(int servings) {
        this.servings = servings;
    }

    public List<String> getIngredients() {
        return List.copyOf(ingredients);
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = List.copyOf(ingredients);
    }

    public List<String> getInstructions() {
        return List.copyOf(instructions);
    }

    public void setInstructions(List<String> instructions) {
        this.instructions = List.copyOf(instructions);
    }

    public boolean isValidFields() {
        return servings>0 && !ingredients.isEmpty() && instructions!=null && title!=null && !title.isEmpty();
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", vegetarian=" + vegetarian +
                ", servings=" + servings +
                ", ingredients=" + ingredients +
                ", instructions=" + instructions +
                '}';
    }
}
