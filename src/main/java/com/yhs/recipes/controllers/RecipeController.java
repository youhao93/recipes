package com.yhs.recipes.controllers;

import com.yhs.recipes.exceptions.RecipeNotFoundException;
import com.yhs.recipes.models.Recipe;
import com.yhs.recipes.services.IRecipeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

/**
 * @author You Hao
 */
@RestController
@RequestMapping("/api/v1/recipes")
public class RecipeController {

    private final IRecipeService service;

    /**
     * Creates a new Recipe controller.
     *
     * @param service The service to get relevant user data from.
     */
    public RecipeController(final IRecipeService service) {
        this.service = service;
    }


    /**
     * Get all recipes.
     * @return List of recipes.
     */
    @GetMapping
    public ResponseEntity<List<Recipe>> getRecipes() {
        try {
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        } catch (RecipeNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Recipe not found");
        }
    }


    /**
     * Get filtered recipes.
     *
     * @param params mapping of filters.
     * @return List of filtered recipes.
     */
    @GetMapping("/search/")
    public ResponseEntity<List<Recipe>> getFilteredRecipes(@RequestParam Map<String, String> params) {
        try {
            return new ResponseEntity<>(service.filtered(params), HttpStatus.OK);
        } catch (RecipeNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Recipe not found");
        }
    }


    /**
     * Get a single recipe based on the given id.
     *
     * @param id The ID of the recipe that gets updated.
     * @return Recipe with its corresponding ID.
     */
    @GetMapping("{id}")
    public ResponseEntity<Recipe> getRecipe(@PathVariable Long id) {
        try {
            if (service.findById(id) != null) {
                return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
            } else {
                throw new RecipeNotFoundException("Recipe not found");
            }
        } catch (RecipeNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Recipe not found");
        }
    }


    /**
     * Creates a single recipe based on the given properties.
     *
     * @param recipe The recipe that needs to be created.
     * @return The created recipe.
     */
    @PostMapping
    public ResponseEntity<Recipe> create(@RequestBody final Recipe recipe) {
            try {
                return new ResponseEntity<>(service.saveAndFlush(recipe), HttpStatus.CREATED);
            } catch (ResponseStatusException exception) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not create recipe");
            }
    }

    /**
     * Updates a single recipe based on the given properties.
     *
     * @param id The id of the recipe that gets updated.
     * @param recipe The new properties of the recipe.
     * @return The updated recipe.
     */
    @PutMapping(value = "{id}")
    public ResponseEntity<Recipe> update(@PathVariable Long id, @RequestBody Recipe recipe) {
        try {
            return new ResponseEntity<>(service.update(id, recipe), HttpStatus.OK);
        } catch (RecipeNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Recipe could not be updated, because existing recipe was not found");
        }
    }


    /**
     * Deletes a single recipe based on the given id.
     *
     * @param id of the recipe that needs to be deleted.
     * @return returns the http status and message if it's successfully deleted.
     */
    @DeleteMapping(value = "{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            service.deleteById(id);
            return new ResponseEntity<>("Recipe successfully deleted", HttpStatus.OK);
        } catch (RecipeNotFoundException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Could not found recipe");
        }
    }

}

