package com.yhs.recipes.services;

import com.yhs.recipes.models.Recipe;

import java.util.List;
import java.util.Map;

public interface IRecipeService {
    List<Recipe> findAll();

    List<Recipe> filtered(Map<String, String> requestParams);

    Recipe findById(Long id);

    Recipe saveAndFlush(Recipe recipe);

    Recipe update(Long id, Recipe recipe);

    void deleteById(Long id);
}
