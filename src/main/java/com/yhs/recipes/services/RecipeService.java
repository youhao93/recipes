package com.yhs.recipes.services;

import com.yhs.recipes.exceptions.InvalidFieldsException;
import com.yhs.recipes.exceptions.RecipeNotFoundException;
import com.yhs.recipes.models.Recipe;
import com.yhs.recipes.repositories.IRecipeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecipeService implements IRecipeService {
    boolean vegetarian;
    int servings;
    String ingredient;
    boolean include = true;
    String instruction;

    private final IRecipeRepository repo;

    public RecipeService(final IRecipeRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Recipe> findAll() {
        return repo.findAll();
    }

    @Override
    public List<Recipe> filtered(Map<String, String> requestParams) {
        List<Recipe> recipes = repo.findAll();
        List<Recipe> filteredRecipes = new ArrayList<>();

        convertListsInRecipeToLowerCase(recipes);
        convertMapToFilters(requestParams);
        if (requestParams.isEmpty()) {
            return recipes;
        } else {
            return filterResults(requestParams, recipes, filteredRecipes);
        }
    }

    private void convertListsInRecipeToLowerCase(List<Recipe> recipes) {
        for (Recipe recipe : recipes) {
            List<String> lowerCaseIngredients = recipe.getIngredients()
                    .stream().map(String::toLowerCase)
                    .collect(Collectors.toList());
            recipe.setIngredients(lowerCaseIngredients);
            List<String> lowerCaseInstructions = recipe.getInstructions()
                    .stream().map(String::toLowerCase)
                    .collect(Collectors.toList());
            recipe.setInstructions(lowerCaseInstructions);
        }
    }

    private List<Recipe> filterResults(Map<String, String> requestParams, List<Recipe> recipes, List<Recipe> filteredRecipes) {
        int i = 0;
        while (i < requestParams.keySet().size()) {
            if (servings > 0) {
                if (filteredRecipes.isEmpty()) {
                    filteredRecipes = recipes.stream()
                            .filter(e -> e.getServings() == servings)
                            .collect(Collectors.toList());
                } else {
                    filteredRecipes = filteredRecipes.stream()
                            .filter(e -> e.getServings() == servings)
                            .collect(Collectors.toList());
                }
            }
            if (ingredient != null && !ingredient.isEmpty()) {
                if (filteredRecipes.isEmpty()) {
                    if (include) {
                        filteredRecipes = recipes.stream()
                                .filter(e -> e.getIngredients().contains(ingredient))
                                .collect(Collectors.toList());
                    } else {
                        filteredRecipes = recipes.stream()
                                .filter(e -> !e.getIngredients().contains(ingredient))
                                .collect(Collectors.toList());
                    }
                } else {
                    if (include) {
                        filteredRecipes = filteredRecipes.stream()
                                .filter(e -> e.getIngredients().contains(ingredient))
                                .collect(Collectors.toList());
                    } else {
                        filteredRecipes = filteredRecipes.stream()
                                .filter(e -> !e.getIngredients().contains(ingredient))
                                .collect(Collectors.toList());
                    }
                }
            }
            if (instruction != null && !instruction.isEmpty()) {
                if (filteredRecipes.isEmpty()) {
                    filteredRecipes = recipes.stream()
                            .filter(e -> e.getInstructions().contains(instruction))
                            .collect(Collectors.toList());
                } else {
                    filteredRecipes = filteredRecipes.stream()
                            .filter(e -> e.getInstructions().contains(instruction))
                            .collect(Collectors.toList());
                }
            }
            if (vegetarian) {
                if (filteredRecipes.isEmpty()) {
                    filteredRecipes = recipes.stream()
                            .filter(Recipe::isVegetarian)
                            .collect(Collectors.toList());
                } else {
                    filteredRecipes = filteredRecipes.stream()
                            .filter(Recipe::isVegetarian)
                            .collect(Collectors.toList());
                }
            } else {
                if (filteredRecipes.isEmpty()) {
                    filteredRecipes = recipes.stream()
                            .filter(e -> !e.isVegetarian())
                            .collect(Collectors.toList());
                } else {
                    filteredRecipes = filteredRecipes.stream()
                            .filter(e -> !e.isVegetarian())
                            .collect(Collectors.toList());
                }
            }
            i++;
        }
        setFiltersToDefault();
        return filteredRecipes;
    }

    private void convertMapToFilters(Map<String, String> requestParams) {
        for (String key : requestParams.keySet()) {
            switch (key.toLowerCase()) {
                case "vegetarian":
                    vegetarian = Boolean.parseBoolean(requestParams.get("vegetarian"));
                    break;
                case "servings":
                    servings = Integer.parseInt(requestParams.get("servings"));
                    break;
                case "ingredient":
                    ingredient = requestParams.get("ingredient").toLowerCase();
                    break;
                case "instruction":
                    instruction = requestParams.get("instruction").toLowerCase();
                    break;
                case "include":
                    include = Boolean.parseBoolean(requestParams.get("include"));
                    break;
            }
        }
    }

    private void setFiltersToDefault() {
        vegetarian = false;
        servings = 0;
        ingredient = "";
        instruction = "";
        include = true;
    }

    @Override
    public Recipe findById(Long id) {
        Optional<Recipe> optional = repo.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Recipe saveAndFlush(Recipe recipe) {
        if (recipe.isValidFields()) {
            return repo.saveAndFlush(recipe);
        } else {
            throw new InvalidFieldsException("Not all fields are present");
        }
    }

    @Override
    public Recipe update(Long id, Recipe recipe) {
        Optional<Recipe> foundRecipe = repo.findById(id);
        if (foundRecipe.isPresent()) {
            Recipe existingRecipe = repo.getReferenceById(id);
            BeanUtils.copyProperties(recipe, existingRecipe, "id");
            return repo.saveAndFlush(existingRecipe);
        } else {
            throw new RecipeNotFoundException("Recipe not found");
        }
    }


    @Override
    public void deleteById(Long id) {
        Optional<Recipe> recipe = repo.findById(id);
        if (recipe.isPresent()) {
            repo.deleteById(id);
        } else {
            throw new RecipeNotFoundException("Recipe not found");
        }
    }
}
