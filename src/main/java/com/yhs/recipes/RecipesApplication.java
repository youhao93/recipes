package com.yhs.recipes;

import com.yhs.recipes.models.Recipe;
import com.yhs.recipes.repositories.IRecipeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class RecipesApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecipesApplication.class, args);
	}

	/**
	 * Fills the database after starting up
	 * the application the first time.
	 * @param repository to save data into the MySQL database
	 * @return
	 */
//	@Bean
//	public CommandLineRunner loadRecipes(IRecipeRepository repository) {
//		return(args) -> {
//			List<String> ingredients = List.of("Tomato", "Rice", "Salt", "Water");
//			List<String> instructions = List.of("1. Cook the water", "2. Put in the pasta", "3. Add the sauce.");
//			Recipe r1 = new Recipe("Tomaten recept", true, 3, ingredients, instructions);
//			Recipe r2 = new Recipe("Eierkoeken", false, 5, ingredients, instructions);
//			Recipe r3 = new Recipe("Rijst", true, 4, ingredients, instructions);
//			Recipe r4 = new Recipe("Pasta gerecht", false, 6, ingredients, instructions);
//			Recipe r5 = new Recipe("Instant noodles", true, 2, ingredients, instructions);
//			repository.saveAndFlush(r1);
//			repository.saveAndFlush(r2);
//			repository.saveAndFlush(r3);
//			repository.saveAndFlush(r4);
//			repository.saveAndFlush(r5);
//		};
//	}
}
