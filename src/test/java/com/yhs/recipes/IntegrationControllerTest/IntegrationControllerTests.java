package com.yhs.recipes.IntegrationControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yhs.recipes.models.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationControllerTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    ObjectMapper objectMapper;
    private Recipe r1;
    private Recipe r2;
    private Recipe r3;
    private Recipe r4;
    private Recipe r5;
    private final String url = "/api/v1/recipes/";

    @BeforeEach
    void setUp() {
        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");

        List<String> ingredients2 = List.of("Cheese", "Tomato", "Salad", "Butter", "Bread");
        List<String> ingredientsWithMeat2 = List.of("Cheese", "Tomato", "Meat", "Butter", "Bread");
        List<String> instructions2 = List.of("Spread out butter on bread", "place cheese and salad on top", "Place the tomato", "Place the cheese");

        r1 = new Recipe(1L, "Pasta linguini", true, 5, ingredients, instructions);
        r2 = new Recipe(2L, "Pasta bolognese", true, 6, ingredients, instructions);
        r3 = new Recipe(3L, "Bread with meat", false, 5, ingredientsWithMeat2, instructions2);
        r4 = new Recipe(4L, "Pasta pesto", true, 4, ingredients, instructions);
        r5 = new Recipe(5L, "Bread with salad", true, 3, ingredients2, instructions2);
    }

    @Test
    public void getRecipeTest() throws Exception {
        mockMvc.perform(get(url + 87)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Eierkoeken"))
                .andExpect(jsonPath("$.servings").value(5))
                .andExpect(jsonPath("$.vegetarian").value(false))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

        mockMvc.perform(get(url + 88)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Rijst"))
                .andExpect(jsonPath("$.servings").value(4))
                .andExpect(jsonPath("$.vegetarian").value(true))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

        mockMvc.perform(get(url + 89)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("Pasta gerecht"))
                .andExpect(jsonPath("$.servings").value(6))
                .andExpect(jsonPath("$.vegetarian").value(false))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

        mockMvc.perform(get(url + 1000)
                        .contentType("application/json"))
                .andExpect(status().isNotFound());

    }

    @Test
    public void updateRecipeTest() throws Exception {
        mockMvc.perform(put(url + 101)
                        .content(objectMapper.writeValueAsString(r5))
                        .contentType("application/json"))
                .andExpect(status().isOk());



    }

    @Test
    public void postRecipeTest() throws Exception {
        mockMvc.perform(post(url)
                        .content(objectMapper.writeValueAsString(r1))
                        .contentType("application/json"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Pasta linguini"))
                .andExpect(jsonPath("$.servings").value(5))
                .andExpect(jsonPath("$.vegetarian").value(true))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

        mockMvc.perform(post(url)
                        .content(objectMapper.writeValueAsString(r2))
                        .contentType("application/json"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Pasta bolognese"))
                .andExpect(jsonPath("$.servings").value(6))
                .andExpect(jsonPath("$.vegetarian").value(true))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

        mockMvc.perform(post(url)
                        .content(objectMapper.writeValueAsString(r3))
                        .contentType("application/json"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("Bread with meat"))
                .andExpect(jsonPath("$.servings").value(5))
                .andExpect(jsonPath("$.vegetarian").value(false))
                .andExpect(jsonPath("$.ingredients").isNotEmpty())
                .andExpect(jsonPath("$.instructions").isNotEmpty());

    }

    @Test
    public void deleteRecipeTest() throws Exception {
        mockMvc.perform(delete(url + 108))
                .andExpect(status().isOk());

        mockMvc.perform(delete(url + 107))
                .andExpect(status().isOk());

        mockMvc.perform(delete(url + 106))
                .andExpect(status().isOk());

        mockMvc.perform(delete(url + 1000))
                .andExpect(status().isNotFound());
    }



    @Test
    public void postBadRequest() throws Exception {
        List<String> ingredients2 = List.of("Cheese", "Tomato", "Salad", "Butter", "Bread");
        List<String> instructions2 = List.of("Spread out butter on bread", "place cheese and salad on top", "Place the tomato", "Place the cheese");
        r3 = new Recipe(3L, "", false, 0, ingredients2, instructions2);
        String url = "/api/v1/recipes/";

        mockMvc.perform(post(url)
                        .content(objectMapper.writeValueAsString(r3))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest());
    }

}
