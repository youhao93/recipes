package com.yhs.recipes.services;

import com.yhs.recipes.exceptions.InvalidFieldsException;
import com.yhs.recipes.exceptions.RecipeNotFoundException;
import com.yhs.recipes.models.Recipe;
import com.yhs.recipes.repositories.IRecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RecipeServiceTest {

    private IRecipeService service;

    @Mock
    private IRecipeRepository repo;

    private Recipe r1;
    private Recipe r2;
    private Recipe r3;
    private Recipe r4;
    private Recipe r5;

    @BeforeEach
    public void setup() {
        service = new RecipeService(repo);

        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");

        List<String> ingredients2 = List.of("Cheese", "Tomato", "Salad", "Butter", "Bread");
        List<String> ingredientsWithMeat2 = List.of("Cheese", "Tomato", "Meat", "Butter", "Bread");
        List<String> instructions2 = List.of("Spread out butter on bread", "place cheese and salad on top", "Place the tomato", "Place the cheese");

        r1 = new Recipe(1L, "Pasta linguini", true, 5, ingredients, instructions);
        r2 = new Recipe(2L, "Pasta bolognese", true, 6, ingredients, instructions);
        r3 = new Recipe(3L, "Bread with meat", false, 5, ingredientsWithMeat2, instructions2);
        r4 = new Recipe(4L, "Pasta pesto", true, 4, ingredients, instructions);
        r5 = new Recipe(5L, "Bread with salad", true, 3, ingredients2, instructions2);
    }

    @Test
    public void getAllRecipesTest() {
        List<Recipe> allRecipes = Arrays.asList(r1, r2, r3, r4, r5);
        when(repo.findAll()).thenReturn(allRecipes);

        List<Recipe> listResult = service.findAll();
        assertEquals(5, listResult.size());
        verify(repo, times(1)).findAll();
    }

    @Test
    public void getRecipeTest() {
        when(repo.findById(1L)).thenReturn(Optional.of(r1));
        Recipe foundRecipe = service.findById(1L);

        assertEquals(1L, foundRecipe.getId());
        assertEquals("Pasta linguini", foundRecipe.getTitle());
        assertTrue(foundRecipe.isVegetarian());
    }

    @Test
    void testNonExistingRecipe() {
        when(repo.findById(100L)).thenReturn(Optional.empty());
        assertNull(service.findById(100L));
    }

    @Test
    public void updateRecipeTest() {
        when(repo.findById(1L)).thenReturn(Optional.of(r1));
        when(repo.getReferenceById(1L)).thenReturn(r1);
        List<String> newIngredients = List.of("new", "ingredients");
        List<String> newInstructions = List.of("step1", "step2", "step3", "step4");
        Recipe r1Updated = new Recipe(1L, "Andere titel", false, 5, newIngredients, newInstructions);

        service.update(1L, r1Updated);
        assertEquals("Andere titel", r1.getTitle());
        assertEquals(5, r1.getServings());
        assertFalse(r1.isVegetarian());
        assertEquals(newIngredients, r1.getIngredients());
        assertEquals(newInstructions, r1.getInstructions());
    }

    @Test
    public void updateRecipeNotFoundTest() {
        when(repo.findById(100L)).thenReturn(Optional.empty());
        List<String> newIngredients = List.of("new", "ingredients");
        List<String> newInstructions = List.of("step1", "step2", "step3", "step4");
        Recipe r1Updated = new Recipe(100L, "Andere titel", false, 5, newIngredients, newInstructions);

        RecipeNotFoundException thrown = assertThrows(RecipeNotFoundException.class,
                () -> service.update(100L, r1Updated),
                "Recipe not found");
        assertEquals("Recipe not found", thrown.getMessage());
    }

    @Test
    public void saveNewRecipeTest() {
        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");
        Recipe r6 = new Recipe(6L, "New Recipe", true, 6, ingredients, instructions);
        when(repo.findById(6L)).thenReturn(Optional.of(r6));
        service.saveAndFlush(r6);
        Recipe found = service.findById(6L);
        assertEquals("New Recipe", found.getTitle());
    }

    @Test
    public void cannotSaveNewRecipeTest() {
        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");
        Recipe r6 = new Recipe(6L, "", true, 0, ingredients, instructions);

        InvalidFieldsException thrown = assertThrows(InvalidFieldsException.class,
                () -> service.saveAndFlush(r6),
                "Not all fields are present");
        assertEquals("Not all fields are present", thrown.getMessage());
    }

    @Test
    public void deleteRecipeTest() {
        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");
        Recipe r1 = new Recipe(1L, "Pasta linguini", true, 6, ingredients, instructions);
        when(repo.findById(1L)).thenReturn(Optional.of(r1));
        service.deleteById(1L);
        when(repo.findById(1L)).thenReturn(Optional.empty());
        assertNull(service.findById(1L));
    }

    @Test
    public void deleteRecipeNotFoundTest() {
        List<String> ingredients = List.of("Pasta", "Olive oil", "Tomatoes", "Garlic");
        List<String> instructions = List.of("Put water in the pan", "Boil the water", "Put pasta in the pan", "Cook until ready");
        Recipe r1 = new Recipe(1L, "Pasta linguini", true, 6, ingredients, instructions);
        when(repo.findById(1L)).thenReturn(Optional.empty());

        RecipeNotFoundException thrown = assertThrows(RecipeNotFoundException.class,
                () -> service.deleteById(1L),
                "Recipe not found");
        assertEquals("Recipe not found", thrown.getMessage());
    }


    @Test
    public void filterRecipeListTest() {
        List<Recipe> allRecipes = Arrays.asList(r1, r2, r3, r4, r5);
        when(repo.findAll()).thenReturn(allRecipes);
        Map<String, String> requestParams = new HashMap<>();

        requestParams.put("servings", "5");
        requestParams.put("vegetarian", "true");
        List<Recipe> listResult = service.filtered(requestParams);
        assertEquals(1, listResult.size());

        requestParams.clear();
        requestParams.put("ingredient", "meat");
        List<Recipe> listWithSalad = service.filtered(requestParams);
        assertEquals(1, listWithSalad.size());

        requestParams.clear();
        List<Recipe> allItems = service.filtered(requestParams);
        assertEquals(5, allItems.size());

        requestParams.clear();
        requestParams.put("ingredient", "salad");
        requestParams.put("vegetarian", "true");
        List<Recipe> listSaladAndVegetarian = service.filtered(requestParams);
        assertEquals(1, listSaladAndVegetarian.size());

        requestParams.clear();
        requestParams.put("servings", "5");
        requestParams.put("vegetarian", "false");
        List<Recipe> listServingsAndVegetarian = service.filtered(requestParams);
        assertEquals(1, listServingsAndVegetarian.size());

        requestParams.clear();
        requestParams.put("servings", "5");
        requestParams.put("ingredient", "Garlic");
        requestParams.put("vegetarian", "true");
        List<Recipe> listServingsAndGarlic = service.filtered(requestParams);
        assertEquals(1, listServingsAndGarlic.size());

        requestParams.clear();
        requestParams.put("servings", "5");
        requestParams.put("ingredient", "Garlic");
        requestParams.put("vegetarian", "true");
        requestParams.put("include", "false");
        List<Recipe> listServingsAndExcludeGarlic = service.filtered(requestParams);
        assertEquals(0, listServingsAndExcludeGarlic.size());

        requestParams.clear();
        requestParams.put("ingredient", "cheese");
        List<Recipe> listIngredientCheese = service.filtered(requestParams);
        assertEquals(1, listIngredientCheese.size());

        requestParams.clear();
        requestParams.put("instruction", "Spread out butter on bread");
        List<Recipe> listInstructionSpreadButter = service.filtered(requestParams);
        assertEquals(1, listInstructionSpreadButter.size());

        requestParams.clear();
        requestParams.put("servings", "5");
        requestParams.put("instruction", "Spread out butter on bread");
        List<Recipe> listServingAndInstruction = service.filtered(requestParams);
        assertEquals(1, listServingAndInstruction.size());

        requestParams.clear();
        requestParams.put("vegetarian", "false");
        List<Recipe> listOfNoVegetarianDishes = service.filtered(requestParams);
        assertEquals(1, listOfNoVegetarianDishes.size());
    }

    @Test
    public void filterRecipeVegetarianListTest() {
        List<Recipe> onlyVegetarian = Arrays.asList(r1, r2, r4, r5);
        when(repo.findAll()).thenReturn(onlyVegetarian);
        Map<String, String> requestParams = new HashMap<>();

        requestParams.put("servings", "5");
        requestParams.put("vegetarian", "true");
        List<Recipe> listServings = service.filtered(requestParams);
        assertEquals(1, listServings.size());

        requestParams.clear();
        requestParams.put("vegetarian", "true");
        requestParams.put("ingredient", "Olive oil");
        List<Recipe> listOliveOil = service.filtered(requestParams);
        assertEquals(3, listOliveOil.size());

        requestParams.clear();
        requestParams.put("ingredient", "cheese");
        requestParams.put("include", "false");
        List<Recipe> listIngredientCheese = service.filtered(requestParams);
        listIngredientCheese.forEach(System.out::println);
        assertEquals(0, listIngredientCheese.size());

        requestParams.clear();
        requestParams.put("vegetarian", "true");
        List<Recipe> listOfVegetarianDishes = service.filtered(requestParams);
        assertEquals(4, listOfVegetarianDishes.size());

        requestParams.clear();
        requestParams.put("ingredient", "Pasta");
        requestParams.put("vegetarian", "true");
        List<Recipe> listOfVegetarianDishesWithPasta = service.filtered(requestParams);
        assertEquals(3, listOfVegetarianDishesWithPasta.size());
    }
}